from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///contacts.db'
db = SQLAlchemy(app)


class Contact(db.Model):
    """
    Esta clase representa la tabla en la base de datos
    """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    email = db.Column(db.String(50), nullable=False)
    phone = db.Column(db.String(11), nullable=False)

    def serialize(self):
        return {
            'id':self.id,
            'name':self.name,
            'email':self.email,
            'phone':self.phone
        }


with app.app_context():
    """
    Migra los modelos a la base de datos de manera automática
    """
    db.create_all()


@app.route('/contacts', methods=['GET'])
def get_contacts():
    contacts = Contact.query.all()
    return jsonify({'contacts': [contact.serialize() for contact in contacts]})


@app.route('/contacts/<int:id>', methods=['GET'])
def get_contact(id):
    contact = Contact.query.get(id)
    if not contact:
        # return jsonify({'message': 'No existe el contacto'}), 204
        return jsonify({'message': 'No existe el contacto'}), 404
    return jsonify(contact.serialize())


@app.route('/contacts/<int:id>', methods=['PUT'])
def edit_contact(id):
    contact = Contact.query.get_or_404(id)
    data = request.get_json()
    if 'name' in data:
        contact.name = data['name']
    if 'email' in data:
        contact.email = data['email']
    if 'phone' in data:
        contact.phone = data['phone']
    db.session.commit()
    if not contact:
        # return jsonify({'message': 'No existe el contacto'}), 204
        return jsonify({'message': 'No existe el contacto'}), 404
    return jsonify({'message': 'contacto actualizado con éxito', 'contact':contact.serialize()}), 201


@app.route('/contacts/<int:id>', methods=['DELETE'])
def delete_contact(id):
    contact = Contact.query.get(id)
    if not contact:
        # return jsonify({'message': 'No existe el contacto'}), 204
        return jsonify({'message': 'No existe el contacto'}), 404
    db.session.delete(contact)
    db.session.commit()
    return jsonify({'message': 'contacto eliminado'})


@app.route('/contacts', methods=['POST'])
def create_contacts():
    data = request.get_json()
    contact = Contact(name=data['name'], email=data['email'], phone=data['phone'])
    db.session.add(contact)
    db.session.commit()
    return jsonify({'message': 'contacto creado con éxito', 'contact': contact.serialize()}), 201
